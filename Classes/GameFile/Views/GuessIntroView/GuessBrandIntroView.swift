//
//  GuessBrandIntroView.swift
//  Gamification
//
//  Created by SKY on 05/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit
enum GuessBrandIntroViewAction {
    case back
    case info
    case playNow
}
class GuessBrandIntroView: UIView {
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var buttonContainerView: UIView!
    //@IBOutlet weak var playButton: UIButton!
    
    var playButtonContainerView = PlayGameButtonViewHelper()
    var tapHandler: ((GuessBrandIntroViewAction)->Void)?
    static func loadXib() -> GuessBrandIntroView {
        return UINib(nibName: "GuessBrandIntroView", bundle: Bundle(for: Self.self)).instantiate(withOwner: self, options: nil).first as! GuessBrandIntroView
    }
    
    func populateView(game: Games?) {
       // localizeLabel()
        guard let game = game else { return }
        //setLabels(game: game)
        playButtonContainerView.loadPlayButtonView(sourceView: buttonContainerView, complition: playButtonViewAction(action:))
        playButtonContainerView.populateView(info: nil, game: game)
    }
    
    func stopTimer() {
        playButtonContainerView.stopTimer()
    }
    
    func playButtonViewAction(action: PlayGameButtonViewAction) {
        tapHandler?(.playNow)
        stopTimer()
    }
}
