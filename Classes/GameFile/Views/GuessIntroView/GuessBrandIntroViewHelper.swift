//
//  GuessBrandIntroViewHelper.swift
//  Gamification
//
//  Created by SKY on 05/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

class GuessBrandIntroViewHelper {
    var view: GuessBrandIntroView?
    
    func loadGuessIntroView(containerView: UIView, game: Games?, complition:((GuessBrandIntroViewAction)->Void)?) {
        removeGuessIntroView()
        view = GuessBrandIntroView.loadXib()
        view?.frame = containerView.bounds
        containerView.addSubview(view!)
        view?.tapHandler = complition
        view?.populateView(game: game)
    }
    func stopTimer() {
        view?.stopTimer()
    }
    func removeGuessIntroView() {
        view?.removeFromSuperview()
        view = nil
    }
    
}
