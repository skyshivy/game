//
//  TermsAndCondHelper.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
enum TermsAndCondType {
    case guessBrand
    case all
}
class TermsAndCondHelper {
    var view: TermsAndCondition?
     
    func displayView(sourceView: UIView,type: TermsAndCondType = .all,isButtonNeed: Bool = true, complition:((TermsAndConditionAction)->Void)?) {
        view = TermsAndCondition.loadXib()
        view?.frame = sourceView.bounds
        view?.termsAndConditionActionHandler = complition
        view?.buttonContainerStackView.isHidden = isButtonNeed
        sourceView.addSubview(view!)
        animateBackground1()
        view?.populateView(type: type)
        if #available(iOS 13.0, *) {
            view?.textView.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }   
    }
    
    func populateView(info: TermsAndCon?) {
        Utility.isRTL() ? view?.textView.makeTextWritingDirectionRightToLeft(true) : view?.textView.makeTextWritingDirectionLeftToRight(true)
        var str = ""
        guard let inf = info?.responseDetail.tncConditionsDTOs else { return }
        var index: Int = 0
        for item in inf {
            index += 1
            //str.append(item.terms.replacingOccurrences(of: "^\\s+|\\s+|\\s+$", with: " ", options: .regularExpression))
            str.append("\(index). ")
            //str.append(item.terms.replacingOccurrences(of: "|", with: "\n"))
            str.append(item.terms.replacingOccurrences(of: "\\n", with: "\n"))
            str.append("\n")
            str.append("\n")
        }
        view?.textView.text = str
        let contentSize = view?.textView.sizeThatFits(view?.textView.bounds.size ?? CGSize(width: 0, height: 0))
        
        if ((contentSize?.height ?? 0) + 100) > UIScreen.main.bounds.height {
            view?.textViewConstraints.constant = UIScreen.main.bounds.height-250////contentSize?.height ?? 0
            view?.textView.isScrollEnabled = true
        } else {
            view?.textView.isScrollEnabled = false
            view?.textViewConstraints.constant = contentSize?.height ?? 0
        }        
    }
    func animateBackground1() {
        
        //self.transform = CGAffineTransform()
        self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        view?.containerView?.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        UIView.animate(withDuration: 0.3, animations: {
            self.view?.containerView?.transform = .identity
            self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }) { (done) in
            
        }
    }
    
    func animateBackground() {
        self.view?.animateBackground(containerView: self.view?.containerView)
    }
    
    func animateAndRemove() {
        self.view?.animateAndRemove(containerView: self.view?.containerView, complition: {
            self.removeView()
        })
    }
    
     private func removeView() {
        view?.removeFromSuperview()
        view = nil
    }
}
