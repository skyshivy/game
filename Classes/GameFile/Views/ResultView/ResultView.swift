//
//  ResultView.swift
//  ShakeAndWin
//
//  Created by SKY on 07/12/20.
//

import UIKit
enum ResultViewAction {
    case ok
    case viewPoins
}
class ResultView: UIView {
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var youEarnedLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var checkPointButton: UIButton!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ownStackView: UIStackView!
    @IBOutlet weak var messageStackView: UIStackView!
    var handler: ((ResultViewAction)->Void)?
    
    static func loadXib() -> ResultView {
        return UINib(nibName: "ResultView", bundle:Bundle(for: ResultView.self)).instantiate(withOwner: self, options: nil).first as! ResultView
    }
    
    func populateView(info: ResultData) {
        messageLabel.isHidden = true
        pointLabel.text = info.ownMessage
        messageStackView.isHidden = true
        viewType(infp: info.resultFor ?? .shakeAndWin)
        setImage(info: info)
    }
    
    func setImage(info: ResultData) {
        guard let imge = info.imageurl else { return }
        DispatchQueue.main.async {
        if imge.contains("http") {
            self.resultImageView.downloadImage(url: info.imageurl ?? "")
        } else {
            let url = Constant.imageUrl+"\(info.imageurl ?? "")"
            self.resultImageView.downloadImage(url: url)
        }
        }
    }
    func viewType(infp: ResultFor) {
        switch infp {
        case .referAndWin:
            self.congratsLabel.isHidden = true
            self.messageLabel.isHidden = true
            self.messageStackView.isHidden = false
            self.ownStackView.isHidden = true
            //self.okButton.isHidden = true
            self.setCornerRadius(radius: 20)
            if #available(iOS 13.0, *) {
                self.resultImageView.image = UIImage(named: "ReferThanks", in: Bundle(for: ResultView.self), with: nil)
            } else {
                self.resultImageView.image = UIImage(named: "ReferThanks")
            }
        default:
            print("dsfsd")
        }
    }
    
    
    func setCornerRadius(radius: CGFloat = 5) {
        self.containerView.layer.cornerRadius = radius
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func okButtonAction() {
        handler?(.ok)
        //print("ok")
    }
    @IBAction func viewPointButtonAction() {
        handler?(.viewPoins)
    }
    
}

