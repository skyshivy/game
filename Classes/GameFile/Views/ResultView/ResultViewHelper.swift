//
//  ResultViewHelper.swift
//  ShakeAndWin
//
//  Created by SKY on 07/12/20.
//

import UIKit

class ResultViewHelper {
    
    var view: ResultView?
    
    func loadResultView(containerView: UIView, info: ResultData, complition:((ResultViewAction)->Void)?) {
        //DispatchQueue.main.async {
            self.removeView()
            self.view = ResultView.loadXib()
            self.view?.frame = containerView.bounds
            self.view?.handler = complition
            self.view?.populateView(info: info)
        //self.view?.animateViewScale(containerView: self.view?.containerView)
            containerView.addSubview(self.view!)
            self.animateBackground()
            self.localize()
        //}
        view?.layoutSubviews()
    }
    
    func localize() {
        view?.congratsLabel.text = "Congratulations!".localizeFor(Self.self)
        view?.youEarnedLabel.text = "You Earned".localizeFor(Self.self)
        view?.okButton.setTitle("Ok".localizeFor(Self.self), for: .normal)
        view?.checkPointButton.setTitle("", for: .normal)
    }
    
    func animateBackground() {
        //view?.backgroundColor = .red
        self.view?.animateViewScale(containerView: view?.containerView)
        
    }
    
    func animateAndRemove() {
        self.view?.animateViewScaleRemove(containerView: self.view?.containerView, complition: {
            self.removeView()
        })
    }
        
    private func removeView() {
        view?.removeFromSuperview()
        view = nil
    }
}

