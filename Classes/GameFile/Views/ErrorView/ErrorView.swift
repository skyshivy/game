//
//  ErrorView.swift
//  ShakeAndWin
//
//  Created by SKY on 14/12/20.
//

import UIKit

class ErrorView: UIView {
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var oopLabel: UILabel!
    @IBOutlet weak var tryNextLabel: UILabel!
    @IBOutlet weak var youCouldWinLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    var complition: (()->Void)?
    static func loadXib() ->ErrorView {
        return UINib(nibName: "ErrorView", bundle:Bundle(for: ErrorView.self)).instantiate(withOwner: self, options: nil).first as! ErrorView
    }
    
    func populateErrorView(info: ErrorData) {
        DispatchQueue.main.async {
            self.setErrorImage(info: info)
            self.tryNextLabel.text = info.message ?? ""
        }
    }
    
    func setErrorImage(info: ErrorData) {
        guard let imge = info.imageUrl else { return }
        if imge.contains("http") {
            errorImageView.downloadImage(url: info.imageUrl ?? "")
        } else {
            let url = Constant.imageUrl+"\(info.imageUrl)"
            errorImageView.downloadImage(url: url)
        }
    }
    
    @IBAction func okButtonAction() {
        complition?()
    }
    
}
