//
//  ErrorViewHelper.swift
//  ShakeAndWin
//
//  Created by SKY on 14/12/20.
//

import UIKit

class ErrorViewHelper {
    var view: ErrorView?
    private var complition: (()->Void)?
    func loadErrorView(containerView: UIView, info: ErrorData, complition: (()->Void)?) {
        
            self.removeView()
            self.view = ErrorView.loadXib()
            self.view?.frame = containerView.bounds
            self.view?.populateErrorView(info: info)
            self.view?.complition = complition
            containerView.addSubview(self.view!)
            self.animateBackground()
            self.localization()
    }
    
    func localization() {
        view?.okButton.setTitle("Ok".localizeFor(Self.self), for: .normal)
        view?.oopLabel.text = "Opps!".localizeFor(Self.self)
        view?.tryNextLabel.text = "Better Luck Next time".localizeFor(Self.self)
        view?.youCouldWinLabel.text = "you couldnt win any reward this time.".localizeFor(Self.self)
    }
    
    func animateBackground() {
        self.view?.animateViewScale(containerView: view?.containerView)
        //self.view?.backgroundColor = .red
    }
    
    func animateAndRemove() {
        self.view?.animateViewScaleRemove(containerView: self.view?.containerView, complition: {
            self.removeView()
        })
    }
        
    private func removeView() {
        view?.removeFromSuperview()
        view = nil
    }
    
}
