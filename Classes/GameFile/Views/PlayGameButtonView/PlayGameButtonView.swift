//
//  PlayGameButtonView.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
enum PlayGameButtonViewAction {
    case playTapped
}
class PlayGameButtonView: UIView {
    @IBOutlet weak var shakeAndWinLabel: UILabel!
    @IBOutlet weak var expireLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    var playGameButtonViewHandler: ((PlayGameButtonViewAction)->Void)?
    var gameList: GameList?
    var game: Games?
    static func loadXib() -> PlayGameButtonView {
        return UINib(nibName: "PlayGameButtonView", bundle:Bundle(for: PlayGameButtonView.self)).instantiate(withOwner: self, options: nil).first as! PlayGameButtonView
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    func populateView(info: GameList?, game: Games?) {
        self.gameList = info
        self.game = game
        settingLabelValue(info: game)
        if game?.executionStatus == "LOCKED" {
            onLock(game: game)
            playButton.isUserInteractionEnabled = false
            playButton.backgroundColor = UIColor.gray
        } else {
            onActive(game: game)
            playButton.isUserInteractionEnabled = true
        }
    }
    
    func settingLabelValue(info: Games?) {
        guard let info = info else { return }
        shakeAndWinLabel.text = info.displayDetails?.name
        discriptionLabel.text = info.displayDetails?.description ?? ""
    }
    
    
    func onActive(game: Games?) {
        //hourAndMinuteAlignment(info: info)
        let date = game?.executionPeriod?.endDateTime ?? ""//info?.responseDetail.games.first?.executionPeriod?.endDateTime ?? ""
        hourMinteAlignmentCheck(date: date, value: "Expires in".localizeFor(Self.self))
    }
    
    func hourMinteAlignmentCheck(date: String, value: String) {
        if Utility.isRTL() {
            let sec = "\((CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).2))" + "sec".localizeFor(Self.self)
            let min = "\((CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).1))" + "min".localizeFor(Self.self)
            let h = "\((CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).0))" + "h".localizeFor(Self.self)
            expireLabel.text = value + " " + h + " " + min + " " + sec
        } else {
            expireLabel.text = value + " \(CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).0)h  \(CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).1)min \(CommomMethod.secondsToHoursMinutesSeconds(seconds: CommomMethod.convertStringIntoDate(date: date)).2)sec"
        }
    }

    func onLock(game: Games?) {
        playButton.backgroundColor = UIColor.gray
        playButton.isUserInteractionEnabled = false
        let date = game?.executionPeriod?.startDateTime ?? ""//info?.responseDetail.games.first?.executionPeriod?.startDateTime ?? ""
        hourMinteAlignmentCheck(date: date, value: "Available in".localizeFor(Self.self))
    }
    
    
//    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
//      return (seconds / 3600, (seconds % 3600) / 60)
//    }
    
    @IBAction func playButtonTapped() {
        playGameButtonViewHandler?(.playTapped)
    }
}
