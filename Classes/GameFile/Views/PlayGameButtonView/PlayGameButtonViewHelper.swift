//
//  PlayGameButtonViewHelper.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
class PlayGameButtonViewHelper {
    var view: PlayGameButtonView?
    var game: Games?
    var timer: Timer?
    var gameList: GameList?
    
    func loadPlayButtonView(sourceView: UIView, complition: ((PlayGameButtonViewAction)->Void)?) {
        removePlayGameButtonView()
        view = PlayGameButtonView.loadXib()
        view?.frame = sourceView.bounds
        view?.playGameButtonViewHandler = complition
        sourceView.addSubview(view!)
        localize()
    }
    
    func localize() {
        view?.playButton.setTitle("Play now".localizeFor(Self.self), for: .normal)
    }
    
    func populateView(info: GameList?, game: Games? = nil) {
        self.gameList = info
        self.game = game
        view?.populateView(info: info, game: game)
        timerStart()
    }

    func timerStart() {
        stopTimer()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        if let info = game {
            populateView(info: nil, game: info)
        }
    }
    
    func stopTimer() {
        //DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = nil
        //}
    }
    
    func  removePlayGameButtonView() {
        view?.removeFromSuperview()
        view = nil
    }
}
