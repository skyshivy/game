//
//  CountDown.swift
//  ShakeAndWin
//
//  Created by SKY on 07/12/20.
//

import Foundation

class CountDown {
    private var timer : Timer?
    private var timeLimit: Int = 0
    private var handler: (()->Void)?
    func startTimer(time: Int = 10, complition:(()->Void)?) {
        timeLimit = time
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeUpdate), userInfo: nil, repeats: true)
        handler = complition
    }
    
    @objc func timeUpdate() {
        if timeLimit<=0 {stopTimer();return}
        timeLimit -= 1
        handler?()
        print("timeUpdate \(timeLimit)")
    }
    
    func stopTimer() {
       // print("timer stopped")
        handler?()
        timer?.invalidate()
        timer = nil
    }
}
