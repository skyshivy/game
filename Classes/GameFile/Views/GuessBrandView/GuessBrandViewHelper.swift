//
//  GuessBrandViewHelper.swift
//  Gamification
//
//  Created by SKY on 06/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit
class GuessBrandViewHelper {
    var view: GuessBrandView?
    
    func loadBrandView(containerView: UIView, info: GuestWin?, complition:((GuessBrandViewAction)->Void)?) {
        removeGuessBrand()
        view = GuessBrandView.loadXib()
        view?.guestBrandHandler = complition
        view?.frame = containerView.bounds
        containerView.addSubview(view!)
        if let data = info {
            view?.populateView(info: data)
        }
    }
    
    func stopTimer() {
        DispatchQueue.main.async {
            self.view?.stopTimer()
        }
        
    }
    
    func removeGuessBrand(){
        view?.removeFromSuperview()
        view = nil
    }
    
}
