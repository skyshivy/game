//
//  ExecuteEvent.swift
//  ShakeAndWin
//
//  Created by SKY on 11/12/20.
//

import Foundation

class ExecuteEvent: Decodable {
    let requestId: String
    let timestamp: String
    let respCode: String
    let respDesc: String
    let responseObject: [RespObj]
}
class RespObj: Decodable {
    let achievmentId: String
    let achievementType: String
    let expiryDate: String
    let displayDetails: [DisplayDetails]
}
