//
//  Result.swift
//  ShakeAndWin
//
//  Created by SKY on 14/12/20.
//

import Foundation
enum ResultFor {
    case shakeAndWin
    case spinAndWin
    case referAndWin
}
struct ResultData {
    var resultFor: ResultFor? = .shakeAndWin
    var imageurl: String?
    var ownMessage: String?
}
class Result: Decodable {
    let requestId: String
    let timestamp: String?
    let respCode: String
    let respDesc: String
    let responseObject: [ResponseObject]?
    let achievement: [ResponseObject]?
}

class ResponseObject: Decodable {
    let achievmentId: String
    let achievementType: String
    let expiryDate: String
    let achievmentStatus: String?
    let productId: String?
    let displayDetails: [DispDetails]
}

class DispDetails : Decodable {
    let lang: String?
    let language: String?
    let name: String
    let description: String
    let synonym: String
    let imageList: [ImgList]?
}
class ImgList: Decodable {
    let imageIdentifier: String?
    let imageType: String?
    let idtype: String?
    let name: String?
}
