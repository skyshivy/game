//
//  GameList.swift
//  ShakeAndWin
//
//  Created by SKY on 10/12/20.
//

import Foundation
class GameList: Decodable {
    var requestId: String
    var timestamp: String
    var respCode: String
    var respDesc: String
    var responseDetail: respDetail
}
class respDetail: Decodable {
    let games: [Games]
}
class Games: Decodable {
    let gameId: String
    let gameTitle: String
    let gameType: String
    let gameStatus: String?
    let executionStatus: String?
    let memberType: String?
    let milestonesAvailable: Bool?
    let leaderboardAvailable: Bool?
    let optinRequired: Bool?
    let timeToComplete: Float?
    let tncRequired: Bool?
    let achievementId: String?
    let validityPeriod: Validity?
    let tncStatus: String?
    let executionPeriod: ExecutionPeriod?
    let displayDetails: DisplayDetails?
    let attributeList: AttributeList?
    let costOfGame: CostOfGame?
    let frequency:[Frequency]
}

class Validity: Decodable {
    let startDateTime: String
    let endDateTime: String
}

class Frequency: Decodable {
    let frequencyValue: String?
    let frequencyType: String?
}

class ExecutionPeriod: Decodable {
    let startDateTime: String?
    let endDateTime: String?
}
class DisplayDetails: Decodable {
    let name: String?
    let description: String
    let synonym: String?
    let lang: String?
    let language: String?
    let imageList: [ImageList]
}
class ImageList: Decodable {
    let name: String?
    let idtype: String?
}
class AttributeList: Decodable {
    let name: String
    let idtype: String
    let id: String?
}
class CostOfGame: Decodable {
    var cost: Float? = 0.0
    let costType: String
}
