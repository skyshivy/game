//
//  UIViewController+Extension.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import UIKit
extension UIViewController {
    func checkLeftToRight() {
        let sementicAttribute: UISemanticContentAttribute = Utility.isRTL() ? .forceRightToLeft : .forceLeftToRight
        
        self.navigationController?.navigationBar.semanticContentAttribute = sementicAttribute
        self.navigationController?.view.semanticContentAttribute = sementicAttribute
        UIView.userInterfaceLayoutDirection(for: sementicAttribute)
        UIView.appearance().semanticContentAttribute = sementicAttribute
        
    }
}
