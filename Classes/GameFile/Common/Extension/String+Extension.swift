//
//  String+Extension.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import Foundation
extension String {
    
    func convertIntIntoHMS() -> String {
        let hours = (Int(self) ?? 0) / 3600
        let minutes = (Int(self) ?? 0) / 60 % 60
        let seconds = (Int(self) ?? 0) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func localizeFor(_ aClass: AnyClass) -> String {
        let bundle = Bundle(for: aClass.self)
        var path = bundle.path(forResource: Utility.currentLanguage(), ofType: "lproj")
        if let path = path {
            //print("path is ±±±±±± \(path)")
        } else {
            path = bundle.path(forResource: "Base", ofType: "lproj")
            //print("path is  \(path)")
        }
        let bundle1 = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle1!, value: "", comment: "")
    }
    
    func getTimeFromString(format: String = "dd-MM-yyyy HH:mm:ss", inFormat: String =  "dd-MM-yyyy") -> String {
        

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from:self)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let finalDate = calendar.date(from:components)!

        dateFormatter.dateFormat = inFormat
            let drawDate = dateFormatter.string(from: finalDate)
            print(drawDate)
        return drawDate
        
    }
    /*
    func localizeForBundle(bundle: Bundle)-> String {
        //let bundle = Bundle(url: <#T##URL#>)//Bundle(for: type(of: self.dy))
        
        var path = bundle.path(forResource: Utility.currentLanguage(), ofType: "lproj")
        if let path = path {
            //print("path is ±±±±±± \(path)")
        } else {
            path = bundle.path(forResource: "Base", ofType: "lproj")
            //print("path is  \(path)")
        }
        let bundle1 = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle1!, value: "", comment: "")
    }
 */
//    var localized: String {
//        // currentLanguageCode is e.g. "en" or "nl"
//        var path = Bundle.main.path(forResource: Utility.currentLanguage(), ofType: "lproj")
//        if let path = path {
//            //print("path is ±±±±±± \(path)")
//        } else {
//            path = Bundle.main.path(forResource: "Base", ofType: "lproj")
//            //print("path is  \(path)")
//        }
//        let bundle = Bundle(path: path!)
//        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
//    }
}
