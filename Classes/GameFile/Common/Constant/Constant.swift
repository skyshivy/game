//
//  Constant.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import Foundation
class Constant {
    static let lottreyRed = TTUtils.uiColor(from: 0xFC4A89)
    static let lottreyGreen = TTUtils.uiColor(from: 0x5CB300)
    static let initialUrl = StoreManager.shared.gameUrl//"49.206.240.154:8150"
    static let spendCircleBackGrndClr = TTUtils.uiColor(from: 0xCF493A)
    static let spendCircleForeGrndClr = TTUtils.uiColor(from: 0xFDEAC6)
    
//MARK:- for production for shakeNWin
//    static let stcGameList = "https://\(initialUrl)/gamification/v1/getGames"
//    static let stcExecuteEvent = "https://\(initialUrl)/gamification/v1/events"
    
//MARK:- for indian server for shakeNWin
    
    static let stcGameList = "http://\(initialUrl)/Gamification-1.0/Gamification/listGames"
    static let stcExecuteEvent = "http://\(initialUrl)/Gamification-1.0/Gamification/executeEvent"

    
    
    //MARK:- for indian server for GuessNWin
    
    static let guessSubmitAnswerStc = guessGetQuestionStc
    
    static let guessGetQuestionStc = "http://\(initialUrl)/Gamification-1.0/Gamification/gameEngine/executeGame/QAGameDetails/"
    
// MARK:- Image url
    static let imageUrl = "http://\(initialUrl)/Gamification-1.0/Gamification/gameEngine/documents/images/"
}
