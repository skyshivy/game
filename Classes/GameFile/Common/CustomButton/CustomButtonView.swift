//
//  CustomButtonView.swift
//  Gamification
//
//  Created by SKY on 03/03/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

class CustomButtonView: UIView {

    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var buttonImageView: UIImageView!
    
    var view: UIView?
    var handle:(()->Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initialSetup()
    }
    
    private func loadXib() -> UIView {
        return UINib(nibName: "CustomButtonView", bundle: Bundle(for: CustomButtonView.self)).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    private func initialSetup() {
        view = loadXib()
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        answerButton.setTitleColor(UIColor.white, for: .normal)
        answerButton.backgroundColor = .clear
        addSubview(view!)
        buttonImageView.isHidden = true
        addBorderWith(color: UIColor.white)
    }
    
    func addBorderWith(color: UIColor) {
        DispatchQueue.main.async {//asyncAfter(deadline: .now()+0.005) {
            self.addBorder(width: 1.5,redius: 5, opacity: 1, color: color)
        }
    }
    
    func resetButton(title: String) {
        answerButton.setTitle(title, for: .normal)
        buttonImageView.isHidden = true
        answerButton.isUserInteractionEnabled = true
        answerButton.setTitleColor(UIColor.white, for: .normal)
        addBorderWith(color: .white)
    }
    
    func wrongButtonSelected() {
        buttonImageView.isHidden = true
        answerButton.isUserInteractionEnabled = false
        buttonImageView.image = UIImage(named: "UncheckBox")
        answerButton.setTitleColor(UIColor.red, for: .normal)
        addBorderWith(color: .red)
    }
    
    func rightButtonSelected() {
        buttonImageView.isHidden = true
        answerButton.isUserInteractionEnabled = false
        buttonImageView.image = UIImage(named: "Checkbox")
        answerButton.setTitleColor(Constant.lottreyGreen, for: .normal)
        addBorderWith(color: .green)
    }
    
    @IBAction func buttonAction() {
       handle?()
    }
}
