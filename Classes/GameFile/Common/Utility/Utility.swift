//
//  Utility.swift
//  Localization
//
//  Created by SKY on 13/06/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//
import UIKit
import Foundation

class Utility {
   private static let APPLE_LANGUAGE_KEY = "AppleLanguages1"
    static func selectEnglishLanguage() {
        UserDefaults.standard.set("en", forKey: APPLE_LANGUAGE_KEY)
        UserDefaults.standard.synchronize()
    }
    
    static func selectArabicLanguage() {
        UserDefaults.standard.set("ar", forKey: APPLE_LANGUAGE_KEY)
        UserDefaults.standard.synchronize()
    }
    static func currentLanguage() -> String {
        return UserDefaults.standard.value(forKey: APPLE_LANGUAGE_KEY) as? String ?? "en"
    }
    
    static func isRTL() -> Bool {
        let lang = UserDefaults.standard.value(forKey: APPLE_LANGUAGE_KEY) as? String ?? "ar"
        return lang == "en" ? false : true
    }
    
    static func switchTheLanguageFromLogin() {
        isRTL() ? selectEnglishLanguage() : selectArabicLanguage()
        flipController(storyboard: "Login", id: "LoginViewController")
    }
    static func switchTheLanguageFromController() {
        isRTL() ? selectEnglishLanguage() : selectArabicLanguage()
        flipController(storyboard: "Main", id: "CustomTabBarController")
    }
   
    static func setTabControllerAsRootViewController() {
        flipController(storyboard: "Main", id: "CustomTabBarController")
    }
    
    static func logoutAction() {
        flipController(storyboard: "Login", id: "LoginViewController")
    }
    
    private static func flipController(storyboard: String, id: String) {
        let transition: UIView.AnimationOptions = isRTL() ?  .transitionFlipFromLeft : .transitionFlipFromRight
        let controller = UIStoryboard(name: storyboard, bundle: Bundle.main)
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.rootViewController = controller.instantiateViewController(withIdentifier: id)
        UIView.transition(with: mainwindow, duration: 0.5, options: transition, animations: nil, completion: nil)
    }
}
