//
//  PopupViewHelper.swift
//  Gamification
//
//  Created by SKY on 09/03/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

enum XibType: String {
    case GuessXib = "GuessPopupView"
}
enum ResultType {
    case success
    case fail
}

class PopupViewHelper {
    
    private static var view: PopupView?
    private static var rootView = UIApplication.shared.windows.last?.rootViewController?.view
    
    static func show(xibType: XibType, info: ErrorData? = nil, resultType: ResultType, complition:((PopupViewAction)->Void)? = nil) {
        removeViewWith(animation: false)
        rootView = UIApplication.shared.windows.last?.rootViewController?.view
        view = PopupView.loadXib(nibName: xibType.rawValue)
        view?.frame = UIScreen.main.bounds
        rootView?.addSubview(view!)
        viewFadeInOut(in1: true)
        view?.handler = complition
        checkType(xibType: xibType, resultType: resultType)
        view?.populateGuessAndWinData(info: info)
    }
    
   static func checkType(xibType: XibType, resultType: ResultType) {
        switch xibType {
        case .GuessXib:
            if resultType == .success{guessSuccessData()}else{guessFailData()}
        default:
            break
        }
    }
    
    static func viewFadeInOut(in1: Bool) {
        view?.alpha =  in1 ? 0.0 : 1.0
        UIView.animate(withDuration: 0.3) {
            self.view?.alpha = in1 ? 1.0 : 0.0
        } completion: { (done) in
            print("sdf")
            if !in1 { removeViewWith(animation: false) }
        }
    }
    
    static func guessSuccessData() {
        view?.guessSuccessData()
    }
    static func guessFailData() {
        view?.guessFailData()
    }
    static func removeViewWith(animation: Bool = true) {
        if animation {
            viewFadeInOut(in1: false)
        } else {
            print("view removed")
            view?.removeFromSuperview()
            view = nil
        }
    }
}
