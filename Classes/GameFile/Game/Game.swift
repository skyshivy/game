//
//  Game.swift
//  ShakeAndWin
//
//  Created by SKY on 11/12/20.
//

import UIKit
public enum GameAction {
    case dismiss
    case onSuccess
    case tokenExpired
}
public class Game {
    internal static var isTokenUpdated: Bool = true
    private static var controller: UIViewController?
    private static var gameType: String?
    private static var req: [String : Any]?
    private static var complition:((GameAction)->Void)?
    
    private class func getUrlFromInfoPlist() {
        var resourceFileDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            resourceFileDictionary = NSDictionary(contentsOfFile: path)
        }
        if let resourceFileDictionaryContent = resourceFileDictionary {
            if let url = resourceFileDictionaryContent.object(forKey: "gameUrl") {
                StoreManager.shared.gameUrl = url as? String ?? ""
            }
        }
    }
    /*
    private class func shakeAndWin(mobileNumber: String, language: String, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        StoreManager.shared.msisdn = mobileNumber
        StoreManager.shared.language = language
        language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        let window = UIApplication.shared.windows.first?.rootViewController
    }
    
    private class func spinAndWin(msisdn: String, language: String, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        StoreManager.shared.msisdn = msisdn
        StoreManager.shared.language = language
        language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        
        let window = UIApplication.shared.windows.first?.rootViewController
        loadDirectSpinViewController(window: window, complition: complition)
        //loadGameListViewController(window: window)
    }
    
    
    
    
    private class func loadDirectActiveViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "Active", bundle: Bundle(for: GameLabelViewController.self)).instantiateViewController(withIdentifier: "GameLabelViewController") as! GameLabelViewController
        controller.complition = complition
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .fullScreen
        //controller.modalPresentationStyle = .fullScreen
        //if let cont  = controller {
        window?.present(nav, animated: true, completion: nil)
    }
    private class func loadDirectSpinViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "Spin", bundle: Bundle(for: SpinViewController.self)).instantiateViewController(withIdentifier: "SpinViewController") as! SpinViewController
        controller.modalPresentationStyle = .fullScreen
        controller.complition = complition
        //if let cont  = controller {
        window?.present(controller, animated: true, completion: nil)
    }
    
    
    
    private class func shakeGame(msisdn: String? = nil, token: String, nationalId: String? = nil, idType: String? = nil, language: String, complition:((GameAction)->Void)?) {
        
        Game.getUrlFromInfoPlist()
        getGameList(mNo: msisdn, language: language, token: token, nId: nationalId, idType: idType)
        return
            language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        let window = UIApplication.shared.windows.first?.rootViewController
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "Shake", bundle: Bundle(for: ShakeViewController.self)).instantiateViewController(withIdentifier: "ShakeViewController") as! ShakeViewController
        
        //instantiateInitialViewController() as? SpinViewController //instantiateViewController(identifier: "SpinViewController")
        controller.complition = complition
        controller.modalPresentationStyle = .fullScreen
        //if let cont  = controller {
        window?.present(controller, animated: true, completion: nil)
    }
    
    private class func loadDirectReferViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "Refer", bundle: Bundle(for: ReferViewController.self)).instantiateViewController(withIdentifier: "ReferViewController") as! ReferViewController
        
        //instantiateInitialViewController() as? SpinViewController //instantiateViewController(identifier: "SpinViewController")
        controller.modalPresentationStyle = .fullScreen
        controller.complition = complition
        //if let cont  = controller {
        window?.present(controller, animated: true, completion: nil)
    }
    
    private class func loadDirectSpendWinViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "SpendWin", bundle: Bundle(for: SpendViewController.self)).instantiateViewController(withIdentifier: "SpendViewController") as! SpendViewController
        controller.modalPresentationStyle = .fullScreen
        controller.complition = complition
        //if let cont  = controller {
        window?.present(controller, animated: true, completion: nil)
    }
    //
    private class func loadDirectLotteryViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "Lottery", bundle: Bundle(for: LotteryGameController.self)).instantiateViewController(withIdentifier: "LotteryGameController") as! LotteryGameController
        controller.modalPresentationStyle = .fullScreen
        controller.complition = complition
        //if let cont  = controller {
        window?.present(controller, animated: true, completion: nil)
    }
    
    private class func loadDirectMyRewardViewController(window: UIViewController?, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        let controller = UIStoryboard(name: "MyReward", bundle: Bundle(for: MyRewardViewController.self)).instantiateViewController(withIdentifier: "MyRewardViewController") as! MyRewardViewController
        controller.complition = complition
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .fullScreen
        //controller.modalPresentationStyle = .fullScreen
        //if let cont  = controller {
        window?.present(nav, animated: true, completion: nil)
    }
    
    private class func startPredictGame(mobileNumber: String, language: String, complition:((GameAction)->Void)?) {
        Game.getUrlFromInfoPlist()
        StoreManager.shared.msisdn = mobileNumber
        StoreManager.shared.language = language
        language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        let window = UIApplication.shared.windows.first?.rootViewController
        
        let controller = UIStoryboard(name: "Predict", bundle: Bundle(for: MatchViewController.self)).instantiateViewController(withIdentifier: "MatchViewController") as! MatchViewController
        controller.complition = complition
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .fullScreen
        //controller.modalPresentationStyle = .fullScreen
        //if let cont  = controller {
        window?.present(nav, animated: true, completion: nil)
    }
    
    private class func getGameList(mNo: String?, language: String, token: String, nId: String? = nil, idType: String? = nil) {
        language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        if let mNo = mNo {
            StoreManager.shared.msisdn = mNo
        } else {
            StoreManager.shared.removeMsisdn()
        }
        
        if let nid = nId {
            StoreManager.shared.nationalId = nid
        } else {
            StoreManager.shared.removeNationalId()
        }
        
        
        StoreManager.shared.language = language
        let queryParam: [[String : String]]!
        if StoreManager.shared.msisdn.isEmpty {
            queryParam = [["key": "PCMR390347", "keyType": "NationalId"],["key": "1", "keyType": "NationalIdType"],["key": StoreManager.shared.language, "keyType": "LANG"]]
        } else {
            queryParam = [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"]]
        }
        
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
            "keyword": "ListGames",
            "queryParams": queryParam
        ] as [String : Any]
        let url = Constant.stcGameList
        NetworkManager.post(myStruct: GameList.self, url: url, request: myDict) { (data, error) in
            print("hello")
        }
    }
 */
    public class func updateToken(token: String) {
        StoreManager.shared.token = token
        Game.isTokenUpdated = true
    }
    
    public class func openGame(controller: UIViewController, mobileNumber: String?,language: String, nationalId: String, nationalIdType: String, gameType: String, gameid: String?, token: String, complition:((GameAction)->Void)?) {
        StoreManager.shared.token = token
        let req = createRequest(mobileNumber: mobileNumber, language: language, nationalId: nationalId, nationalIdType: nationalIdType, gameType: gameType, gameid: gameid)
        
        getGameList1(controller: controller, gameType: gameType, req: req, complition: complition)
    }
    
    private class func getGameList1(controller: UIViewController, gameType: String, req: [String : Any], complition:((GameAction)->Void)?) {
       // Game.reqData = req
        Game.controller = controller
        Game.gameType = gameType
        Game.req = req
        Game.complition = complition
        print("crate game \(req)")
        let url = Constant.stcGameList
        NetworkManager.post(myStruct: GameList.self, url: url, request: req) { (data, error) in
            if let data = data {
                loadGameType(controller: controller, info: data, gameType: gameType, complition: complition)
            } else {
                print("error is \(error)")
                    complition?(.tokenExpired)
            }
        }
    }
    
    private class func loadGameType(controller: UIViewController, info: GameList, gameType: String, complition:((GameAction)->Void)?) {
        if info.respCode == "SC0000" {
            guard let game = info.responseDetail.games.first else {return}
            if gameType == "ShakeNWin" {
                shakeBoxGame(controller: controller, game: game, complition: complition)
            } else if gameType == "GuessNWin" {
                loadGuessBrandViewController( controller: controller, game: game, complition: complition)
            }
        } else {
            errorFound()
        }
    }
    
    private class func errorFound() {
        
    }
    
    private class func createRequest(mobileNumber: String?, language: String, nationalId: String, nationalIdType: String, gameType: String, gameid: String?)-> Dictionary<String, Any> {
        
        Game.getUrlFromInfoPlist()
        StoreManager.shared.nationalId = nationalId
        StoreManager.shared.nationalIdType = nationalIdType
        StoreManager.shared.msisdn = mobileNumber ?? ""
        StoreManager.shared.language = language
        language == "AR" ? Utility.selectArabicLanguage() : Utility.selectEnglishLanguage()
        
        if let mNo = mobileNumber {
            StoreManager.shared.msisdn = mNo
        } else {
            StoreManager.shared.removeMsisdn()
        }
        if let gId = gameid {
            StoreManager.shared.gameId = gId
        } else  {
            StoreManager.shared.removeGameId()
        }
        
        let queryParam: [[String : String]]!
        let searchQuerry: [[String: String]]!
        
        if let gid = gameid {
            searchQuerry = [["filterOn": "GameId",
                             "filterValue": gid]]
        } else {
            searchQuerry = [["filterOn": "GameType",
                             "filterValue": gameType]]
        }
        
        if let mNo = mobileNumber {
            queryParam = [["key": mNo, "keyType": "Msisdn"],["key": nationalId, "keyType": "NationalId"],["key": nationalIdType, "keyType": "NationalIdType"],["key": language, "keyType": "LANG"]]
        } else {
            queryParam = [["key": nationalId, "keyType": "NationalId"],["key": nationalIdType, "keyType": "NationalIdType"],["key": language, "keyType": "LANG"]]
        }
        
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
            "keyword": "ListGames",
            "queryParams": queryParam as Any,
            "searchFilter": searchQuerry as Any
        ] as [String : Any]
        
        return myDict
    }
    
    private class func shakeBoxGame(controller: UIViewController, game: Games, complition:((GameAction)->Void)?) {
        DispatchQueue.main.async {
            
            let controller1 = UIStoryboard(name: "ShakeBox", bundle: Bundle(for: ShakeBoxViewController.self)).instantiateViewController(withIdentifier: "ShakeBoxViewController") as! ShakeBoxViewController
            controller1.modalPresentationStyle = .fullScreen
            controller1.complition = complition
            controller1.game = game
            controller.present(controller1, animated: true, completion: nil)
        }
    }
    
    private class func loadGuessBrandViewController(controller: UIViewController,game:Games, complition:((GameAction)->Void)?) {
        DispatchQueue.main.async {
        let controller1 = UIStoryboard(name: "GuessBrand", bundle: Bundle(for: GuessBrandController.self)).instantiateViewController(withIdentifier: "GuessBrandController") as! GuessBrandController//.instantiateInitialViewController() as? GuessBrandController //instantiateViewController(identifier: "SpinViewController")
        controller1.game = game
        controller1.modalPresentationStyle = .fullScreen
        controller1.complition = complition
        controller.present(controller1, animated: true, completion: nil)
        }
    }
}
