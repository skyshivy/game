//
//  GuessBrandController.swift
//  Gamification
//
//  Created by SKY on 05/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

class GuessBrandController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var gameNameLabel: UILabel!
    var guessBrandIntroViewHelper = GuessBrandIntroViewHelper()
    var guessBrandViewHelper = GuessBrandViewHelper()
    var termsAndCondHelper = TermsAndCondHelper()
    var resultVirw = ResultViewHelper()
    var errorView = ErrorViewHelper()
    var gameListViewModel: GameListViewModel?
    var termsAndCon: TermsAndCon?
    //var guessBrandViewHelper = GuessBrandViewHelper()
    var game: Games?
    var guessData: GuestWin?
    var mileStone: String = "1"
    var complition:((GameAction)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        self.checkLeftToRight()
        getQuestionList()
        labelLocalization()
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guessBrandIntroViewHelper.stopTimer()
    }
    func labelLocalization() {
        gameNameLabel.text = game?.gameTitle
    }
    
    func initialSetup() {
        
        gameListViewModel = GameListViewModel()
        getTermsAndConditonApi()
        guessBrandIntroViewHelper.loadGuessIntroView(containerView: self.containerView, game: self.game, complition: guessBrandHandler(action:))
        backButtonSetup()
    }
    
    func backButtonSetup() {
        let backImg = Utility.isRTL() ? "BackButtonL" : "BackButtonR"
        backButton.setImage(UIImage(named: backImg, in: Bundle(for: GuessBrandController.self), compatibleWith: nil), for: .normal)
    }
    
    
    func guessBrandHandler(action: GuessBrandIntroViewAction) {
        switch action {
        case .back:
            print("Back button tapped")
        case .playNow:
            loadGuessView()
        case .info:
            print("info button tapped")
        }
    }
    
    func loadGuessView() {
        
        if let data  = self.guessData {
            guessBrandIntroViewHelper.removeGuessIntroView()
            guessBrandViewHelper.loadBrandView(containerView: self.containerView, info: data, complition: brandViewAcionHandler(action:))
        } else {
            print("Data is nil")
        }
    }
    
    func brandViewAcionHandler(action: GuessBrandViewAction) {
        switch action {
        case .timeOver(let spentTime, let inf):
            print("spent time \(spentTime)")
            apiCallForSubmitAnswer(spentTime: spentTime, info: inf)
        case .gameOver(let spentTime, let inf):
            print("game over spent time \(spentTime)")
            apiCallForSubmitAnswer(spentTime: spentTime, info: inf)
        default:
            print("do nothing")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        guessBrandViewHelper.stopTimer()
    }
    
    
    
    func displayAlert(info: GuessSubmitAns) {
        if info.respCode == "SC0000" {
            let mess = info.responseObject?.first?.displayDetails.first?.description
            let imgData = info.responseObject?.first?.displayDetails.first?.imageList.first?.name
            let data = ErrorData(message: mess, imageUrl: imgData)
            DispatchQueue.main.async {
                PopupViewHelper.show(xibType: .GuessXib, info: data ,resultType: .success) { (action) in
                    PopupViewHelper.removeViewWith(animation: true)
                    self.complition?(.onSuccess)
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                self.complition?(.onSuccess)
            }
        } else {
            let mess = info.responseObject?.first?.displayDetails.first?.description
            let imgData = info.responseObject?.first?.displayDetails.first?.imageList.first?.name
            let data = ErrorData(message: mess, imageUrl: imgData)
            DispatchQueue.main.async {
               
                PopupViewHelper.show(xibType: .GuessXib, info: data, resultType: .fail) { (action) in
                    PopupViewHelper.removeViewWith(animation: true)
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func backButtonAction() {
        self.dismiss(animated: true, completion: nil)
        complition?(.dismiss)
    }
    @IBAction func infoButtonAction() {
        termsAndCondHelper.displayView(sourceView: self.view,type: .all, complition: termsAndCondHelperHandler(action:))
        termsAndCondHelper.populateView(info: termsAndCon)
    }
    func termsAndCondHelperHandler(action: TermsAndConditionAction) {
        switch action {
        case .agree:
            print("")
            termsAndCondHelper.animateAndRemove()
            self.acceptTermAndCondition()
        case .close:
            print("")
            termsAndCondHelper.animateAndRemove()
            
        case .dismissed:
            print("")
        default:
            print(" ")
        }
    }
    
}
// MARK:- Api calls
extension GuessBrandController {
    func getQuestionList() {
        ///gamification/getExecuteGameAPI/1.0/1976/30/QAGameDetails
        ///4/1/QAGameDetails
        
        
        let end = "\(game?.gameId ?? "90")/\(mileStone)"
        let url = Constant.guessGetQuestionStc+end
        var urlRe = URLRequest(url: URL(string: url)!)
        urlRe.setValue(StoreManager.shared.msisdn, forHTTPHeaderField: "MSISDN")
        urlRe.setValue(StoreManager.shared.language, forHTTPHeaderField: "Language")
        urlRe.setValue(Common.getRandonNo(), forHTTPHeaderField: "RequestId")
        urlRe.setValue(StoreManager.shared.nationalId, forHTTPHeaderField: "NationalId")
        urlRe.setValue(StoreManager.shared.nationalIdType, forHTTPHeaderField: "NationalIdType")
        print("urls req ois \(urlRe)")
        NetworkManager.getWithHeader(myStruct: GuestWin.self, urlReq: urlRe) { (data, error) in
            if let data = data {
                self.guessData = data
            } else {
                print("error is \(error)")
            }
        }
    }
    func apiCallForSubmitAnswer(spentTime: Int,info: GuestWin?) {
    
        var index: Int = 0
        let totalQuest = info?.questionaire?.count ?? 0
        for item in 0..<totalQuest {
            if let q = info?.questionaire?[item].answered {
                if q {
                    index += 1
                }
            }
        }
        let timeUtilized: Int = spentTime
        let correctAnswer: String = "\(index)"
        let end = "\(game?.gameId ?? "286")/\(mileStone)"
        let url = Constant.guessSubmitAnswerStc+end
        var req = URLRequest(url: URL(string: url)!)
        req.setValue(StoreManager.shared.nationalIdType, forHTTPHeaderField: "NationalIdType")
        req.setValue(StoreManager.shared.nationalId, forHTTPHeaderField: "NationalId")
        req.setValue(Common.getRandonNo(), forHTTPHeaderField: "requestId")
        req.setValue(StoreManager.shared.msisdn, forHTTPHeaderField: "Msisdn")
        req.setValue(StoreManager.shared.language, forHTTPHeaderField: "Language")
        //req.setValue("submitAnswers", forHTTPHeaderField: "operationId")

        var valueDict: [Dictionary] = []  as [[String : Any]]
        for i in 0..<(info?.questionaire?.count ?? 0) {
            let qId = info?.questionaire?[i].questionId ?? ""
            let oId = info?.questionaire?[i].optionId ?? ""
            valueDict.append(["questionId":qId,"optionId":oId] as [String : Any])
        }

        let myDict: Dictionary = ["Id": info?.id ?? "4","timeUtilized": timeUtilized,"correctAnswers": correctAnswer, "totalQuestions": info?.numberOfQuestions ?? 2,"requestObject": valueDict] as [String : Any]
        NetworkManager.postWithHeader(myStruct: GuessSubmitAns.self, urlReq: req, request: myDict) { (data, error) in
            if let data = data {
                self.displayAlert(info: data)
            }
        }
    }
    
    func getTermsAndConditonApi() {
        gameListViewModel?.getTermsAndCondition(url: Constant.stcExecuteEvent,gameId: game?.gameId ?? "286",complition: { (terms) in
            self.termsAndCon = terms
        })
    }
    
    func acceptTermAndCondition() {
        let url = Constant.stcExecuteEvent
        
        gameListViewModel?.acceptAndCondition(url: url,gameId: game?.gameId ?? "286",complition: { (terms) in
        })
    }
    
}
