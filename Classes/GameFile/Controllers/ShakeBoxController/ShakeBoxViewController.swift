//
//  ShakeBoxViewController.swift
//  Gamification
//
//  Created by SKY on 04/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

class ShakeBoxViewController: UIViewController {
    
    @IBOutlet weak var centerContainerView: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var shakeBoxViewHelper = ShakeBoxViewHelper()
    var termsAndCondHelper = TermsAndCondHelper()
    
    var playGameButtonViewHelper = PlayGameButtonViewHelper()
    var gameOverLayViewHelper = GameOverLayViewHelper()
    var resultViewHelper = ResultViewHelper()
    var errorViewHelper = ErrorViewHelper()
    var timer = CountDown()
    var startShake: Bool = false
    var checkShakeForTime: Int = 3
    var isShaked: Bool = false
    var executeEventViewModel: ExecuteEventViewModel?
    var gameListViewModel: GameListViewModel?
    var termsAndCon: TermsAndCon?
    var gameList: GameList?
    var game: Games?
    var complition:((GameAction)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        localize()
        self.checkLeftToRight()
        getUrlFromInfoPlist()
        playGameButtonViewHelper.view?.isUserInteractionEnabled = false
        getTermsAndConditonApi()
        shakeInitialization()
        //        executeEventViewModel = ExecuteEventViewModel()
        //        executeEventViewModel?.executeEvent(complition: {
        //           // print("dsfddfgdg")
        //        })
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    
    func localize() {
        let imgeName = UIImage(named: Utility.isRTL() ? "BackButtonL" : "BackButtonR", in: Bundle(for: ShakeBoxViewController.self), compatibleWith: nil)
        shakeLabel.text = game?.gameTitle
        backButton.setTitle("Play now".localizeFor(Self.self), for:  .normal)
        backButton.setImage(imgeName, for: .normal)
    }
    
    func getUrlFromInfoPlist() {
        var resourceFileDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            resourceFileDictionary = NSDictionary(contentsOfFile: path)
        }
        if let resourceFileDictionaryContent = resourceFileDictionary {
            if let url = resourceFileDictionaryContent.object(forKey: "gameUrl") {
                StoreManager.shared.gameUrl = url as? String ?? ""
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        playGameButtonViewHelper.stopTimer()
    }
    
    func shakeInitialization() {
        self.becomeFirstResponder()
    }
    
    
    func acceptTermAndCondition() {
        let url = Constant.stcExecuteEvent
        gameListViewModel?.acceptAndCondition(url: url,gameId: "24",complition: { (terms) in
            //            print("terms \(self.termsAndCon)")
            //            print("==============")
        })
    }
    
    func getTermsAndConditonApi() {
        let url = Constant.stcExecuteEvent
        gameListViewModel?.getTermsAndCondition(url: url,gameId: game?.gameId ?? "24",complition: { (terms) in
            self.termsAndCon = terms
            //            print("terms \(self.termsAndCon)")
            //            print("==============")
        })
    }
    
    
    
    func initialSetup() {
        
        startShake = false
        self.isShaked = false
        
        gameListViewModel = GameListViewModel()
        gameOverLayViewHelper.removeGameOverLayViewView()
        
        DispatchQueue.main.async {
            self.playGameButtonViewHelper.populateView(info: self.gameListViewModel?.gameList, game: self.game)
            self.playGameButtonViewHelper.view?.isUserInteractionEnabled = true
        }
        
        shakeBoxViewHelper.loadShakeBoxView(sourceView: centerContainerView, type: .jump)
        playGameButtonViewHelper.loadPlayButtonView(sourceView: buttonContainerView,complition: playButtonContainerHandler(action:))
        if let info = gameList {
            playGameButtonViewHelper.populateView(info: info)
        }
    }
    
    func playButtonContainerHandler(action: PlayGameButtonViewAction) {
        self.onPlayButtonTapAction()
        
        /*
         if self.termsAndCon?.respCode == "SC0000" {
         
         return
         }
         termsAndCondHelper.displayView(sourceView: self.view, complition: termsAndCondHelperHandler(action:))
         termsAndCondHelper.populateView(info: termsAndCon)
         */
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    @IBAction func backButtonAction() {
        complition?(.dismiss)
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
            //self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func shakeAndWinInfoButtonAction() {
        //print("shakeAndWinInfoButtonAction")
        termsAndCondHelper.displayView(sourceView: self.view, isButtonNeed: true, complition: termsAndCondHelperHandler(action:))
        termsAndCondHelper.populateView(info: termsAndCon)
    }
    
    func onPlayButtonTapAction() {
        startShake = true
        timer.startTimer(time: 4) {
            if self.isShaked { return }
            DispatchQueue.main.async {
                self.gameOverLayViewHelper.isShakeBox = true
                self.gameOverLayViewHelper.loadGameOverLayView(sourceView: self.view, complition: self.gameOverLayViewActionHandler(action:))
            }
        }
        //termsAndCondHelper.animateAndRemove()
        playGameButtonViewHelper.removePlayGameButtonView()
    }
}
extension ShakeBoxViewController {
    func gameOverLayViewActionHandler(action: GameOverLayViewAction) {
        switch action {
        case .gotIt:
            gameOverLayViewHelper.removeGameOverLayViewView()
        default:
            print("")
        }
    }
    
    func termsAndCondHelperHandler(action: TermsAndConditionAction) {
        switch action {
        case .agree:
            print("")
            termsAndCondHelper.animateAndRemove()
            self.acceptTermAndCondition()
        case .close:
            print("")
            termsAndCondHelper.animateAndRemove()
        case .dismissed:
            print("")
        default:
            print(" ")
        }
    }
    
    func resultViewHandler(action: ResultViewAction) {
        switch action {
        case .ok:
            
            self.resultViewHelper.animateAndRemove()
            initialSetup()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
                self.dismiss(animated: true, completion: nil)
            })
        case .viewPoins:
            print("")
            
        default:
            print("")
        }
    }
}
extension ShakeBoxViewController {
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        isShaked = true
        if !startShake{ return }
        print("", motion.hashValue)
        
        if (motion == .motionShake) {
            print("")
        }
        
    }
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        isShaked = true
        gameOverLayViewHelper.removeGameOverLayViewView()
        if !startShake{ return }
        shakeBoxViewHelper.loadShakeBoxView(sourceView: self.centerContainerView, type: .shake, fast: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.shakeBoxViewHelper.loadShakeBoxView(sourceView: self.centerContainerView, type: .open) {
                self.startShake = false
                let url = Constant.stcExecuteEvent
                self.gameListViewModel?.assignRewards(url: url,gameId: self.game?.gameId ?? "24",complition: { (data) in
                    DispatchQueue.main.async {
                        if data.respCode == "SC0000" {
                            self.onSuccess(info: data)
                        } else {
                            self.onfailure(info: data)
                        }
                    }
                })
            }
        }
        
    }
    
    func onSuccess(info: Result) {
        self.complition?(.onSuccess)
        let message = info.responseObject?.first?.displayDetails.first?.name
        let urlStr = info.responseObject?.first?.displayDetails.first?.imageList?.first?.name
        self.resultViewHelper.loadResultView(containerView: self.view, info: ResultData(imageurl: urlStr, ownMessage: message), complition: self.resultViewHandler(action:))
    }
    func onfailure(info: Result) {
        let message = info.responseObject?.first?.displayDetails.first?.description
        let imgUrl = info.responseObject?.first?.displayDetails.first?.imageList?.first?.name
        self.errorViewHelper.loadErrorView(containerView: self.view, info: ErrorData(message: message,imageUrl: imgUrl)) {
            self.errorViewHelper.animateAndRemove()
            self.initialSetup()
        }
    }
}

