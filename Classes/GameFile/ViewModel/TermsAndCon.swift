//
//  TermsAndCondition.swift
//  ShakeAndWin
//
//  Created by SKY on 13/12/20.
//

import Foundation
class TermsAndCon: Decodable {
    let requestId: String
    let timestamp: String
    let respCode: String
    let respDesc: String
    let responseDetail: RespDetail
}
class RespDetail: Decodable {
    let tncConditionsDTOs: [TnCConditionsDTOs]
}
class TnCConditionsDTOs: Decodable {
    let id: String
    let terms: String
}
