//
//  GameListViewModel.swift
//  ShakeAndWin
//
//  Created by SKY on 11/12/20.
//

import Foundation
class GameListViewModel {
    var gameList: GameList?
    func getGameList(url: String, complition:(()->Void)?) {
        
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
                "keyword": "ListGames",
            "queryParams": [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"], ["key": "ShakeNWin", "keyType": "GameType"]]
            
        ] as [String : Any]
        
        NetworkManager.post(myStruct: GameList.self, url: url, request: myDict) { (data, error) in
            if let _ = data {
                self.gameList = data
                complition?()
            } else {
                print("Error found json parsing")
            }
        }
    }
    
    func getTermsAndCondition(url: String, gameId: String?,complition:((TermsAndCon)->Void)?) {

        let queryParams: [[String: String]]!
        if StoreManager.shared.msisdn.isEmpty {
            queryParams = [["key": StoreManager.shared.language, "keyType": "LANG"],
                         ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                         ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                         ["key": gameId ?? "-1", "keyType": "Game"], ["key": "view", "keyType": "TNCView"]]
        } else {
            queryParams = [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"],
                        ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                        ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                        ["key": gameId ?? "-1", "keyType": "Game"], ["key": "view", "keyType": "TNCView"]]
        }
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
                "keyword": "executeEvent",
            "queryParams": queryParams
            
        ] as [String : Any]
        NetworkManager.post(myStruct: TermsAndCon.self, url: url, request: myDict) { (data, error) in
            //print("")
            if let da = data {
                
                complition?(da)
            } else {
                print("Error found")
            }
//            print("data is ", data)
//            print("Error is ", error)
        }
    }
    
    func acceptAndCondition(url: String,gameId: String? = nil,complition:((TermsAndCon)->Void)?) {
        let url1 = Constant.stcExecuteEvent
        let queryParams: [[String: String]]!
        if StoreManager.shared.msisdn.isEmpty {
            queryParams = [["key": StoreManager.shared.language, "keyType": "LANG"],
                           ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                           ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                           ["key": gameId ?? "-1", "keyType": "Game"], ["key": "Accept", "keyType": "TNCView"]]
        } else {
            queryParams = [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"],
                           ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                           ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                           ["key": gameId ?? "-1", "keyType": "Game"], ["key": "Accept", "keyType": "TNCView"]]
        }
        
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
                "keyword": "executeEvent",
            "queryParams": queryParams
            
        ] as [String : Any]
        NetworkManager.post(myStruct: TermsAndCon.self, url: url, request: myDict) { (data, error) in
            //print("")
            if let da = data {
                
                complition?(da)
            } else {
                print("Error found")
            }
//            print("data is ", data)
//            print("Error is ", error)
        }
    }
    
    func assignRewards(url: String, gameId: String? = nil,complition:((Result)->Void)?) {
       // print("sdfdfdf")
        let url1 = Constant.stcExecuteEvent
        let queryParams: [[String: String]]!
        if StoreManager.shared.msisdn.isEmpty {
            queryParams = [["key": StoreManager.shared.language, "keyType": "LANG"],
                           ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                           ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                           ["key": gameId ?? "-1", "keyType": "Game"], ["key": "AssignReward", "keyType": "Activity"]]
        } else {
            queryParams = [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"],
                           ["key": StoreManager.shared.nationalId, "keyType": "NationalId"],
                           ["key": StoreManager.shared.nationalIdType, "keyType": "NationalIdType"],
                           ["key": gameId ?? "-1", "keyType": "Game"], ["key": "AssignReward", "keyType": "Activity"]]
        }
        
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
                "keyword": "executeEvent",
            "queryParams":queryParams
            //[["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"], ["key": gameId ?? "24", "keyType": "GAME"], ["key": "1341", "keyType": "MILESTONE"], ]
            
        ] as [String : Any]
        NetworkManager.post(myStruct: Result.self, url: url, request: myDict) { (data, error) in
           // print("")
            if let da = data {
                
                complition?(da)
            } else {
                print("Error found")
            }
//            print("data is ", data)
//            print("Error is ", error)
        }
    }
}
