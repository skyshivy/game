# Game

[![CI Status](https://img.shields.io/travis/shivysky/Game.svg?style=flat)](https://travis-ci.org/shivysky/Game)
[![Version](https://img.shields.io/cocoapods/v/Game.svg?style=flat)](https://cocoapods.org/pods/Game)
[![License](https://img.shields.io/cocoapods/l/Game.svg?style=flat)](https://cocoapods.org/pods/Game)
[![Platform](https://img.shields.io/cocoapods/p/Game.svg?style=flat)](https://cocoapods.org/pods/Game)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Game is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Game'
```

## Author

shivysky, shiv.yadav@6dtech.co.in

## License

Game is available under the MIT license. See the LICENSE file for more info.
